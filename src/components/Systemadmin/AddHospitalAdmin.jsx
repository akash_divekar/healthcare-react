import React from 'react';
import axios from 'axios';
import './SystemAdmin.css';
import Urls from '../../Urls/Urls';

import UserProfile from '../../UserProfile';

class AddHospitalAdmin extends React.Component{

state={hospitals:[],hospitaladmins:[],hid1:null,haid1:null,check:null}

changinghid = (e) =>{
	this.setState({hid1:e.target.value});
}

changinghaid = (e) =>{
	this.setState({haid1:e.target.value})
}

componentDidMount = async() =>{
	this.fetch();
}

fetch = async() =>{
	const req = Urls.getHospitals();
	const res =  await axios.get(req.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
	this.setState({hospitals:res.data});

	console.log(this.state.hospitals);

	const req1 = Urls.getHospitalAdmin();
	const res1 =  await axios.get(req1.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
	this.setState({hospitaladmins:res1.data});

	console.log(this.state.hospitaladmins);

}

submittingthedetailes = async(event) =>{
	event.preventDefault();
		const req = Urls.addhostohosadmin();
		const res =  await axios.post(req.url,{hid:this.state.hid1,haid:this.state.haid1},{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
		console.log(res.data,this.state.hid1,this.state.haid1);
		this.setState({check:true})
		this.fetch();
}

render(){
	return(
	<div className="AddHospitalAdmin">
	<h1>Add Hospital Admin</h1>
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<label style={{color:''}}>Select Hospital :</label>

	<select class="form-control" value={this.state.hid1} onChange={this.changinghid}>
	<option>Select Hospital</option>
	{this.state.hospitals.map((i)=>(
	<option class="form-control" value={i.id}>{i.name}</option>
	))}
	</select>

	<br /><br />

	<label style={{color:''}}>Select Hospital Admin :</label>
	
	<select class="form-control" value={this.state.haid1} onChange={this.changinghaid}>
	<option>Select Hospital Admin</option>
	{this.state.hospitaladmins.map((i)=>(
	<option class="form-control" value={i.id}>{i.name}</option>
	))}
	</select>

	<br /><br />

	<button type="submit" class="btn btn-primary" onClick={this.submittingthedetailes}>Add</button>

	</form>

	{this.state.check ?<div> Hospital admin added successfully</div>:<div></div>}
	
	</div>
	)
	}
	}
export default AddHospitalAdmin;