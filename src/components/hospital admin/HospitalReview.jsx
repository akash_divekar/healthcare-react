import React from 'react';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import UserProfile from '../../UserProfile';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class HospitalReview extends React.Component {

    state = {
        hospitalReviews:[]
    }

    componentDidMount = () => {
        this.getHospitalReviews();
    }

    getHospitalReviews = async() => {
        const request = AllApi.fetchHospitalReviews();
        const response = await Axios.get(request.url,
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        this.setState({hospitalReviews:response.data});
        console.log(this.state.hospitalReviews);
    }

    displayHospitalReviews = () => (
        this.state.hospitalReviews.map(r=>{
            return(
                <TableRow key={r.id}>
                <TableCell>{r.patient.name}</TableCell>
                <TableCell>{r.rating}</TableCell>
                <TableCell>{r.review}</TableCell>
                </TableRow>
            )
        })
    )

    render() {
        const { hospitalReviews } = this.state;
        return (
            <div>
                <h1>Reviews</h1>
                <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Patient Name</TableCell>
                        <TableCell>Rating</TableCell>
                        <TableCell>Review</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        {hospitalReviews.length ? this.displayHospitalReviews() : <TableRow><TableCell align="right">No Reviews present</TableCell></TableRow>}
                    </TableBody>
                    </Table>
            </TableContainer>
            </div>
        )
    }

}

export default HospitalReview;