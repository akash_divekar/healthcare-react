import React from 'react';
import { Button } from 'antd';
import axios from 'axios';
import './Styles.css';
import { Link } from "react-router-dom";
import Urls from '../../Urls/Urls';

class RegisterforDoctor extends React.Component{

state={values:null,username:'',name:'',password:'',experience:'',speciality:'',checkregister:false}

changeofusername = event =>{
	this.setState({username:event.target.value});
}

changeofname = event =>{
	this.setState({name:event.target.value});
}

changeofpassword = event =>{
	this.setState({password:event.target.value});
}

changeofexperience = event =>{
	this.setState({experience:event.target.value});
}

changeofspeciality = event =>{
	this.setState({speciality:event.target.value});
}

submittingthedetailes = async(event) =>{
		event.preventDefault();
		const req = Urls.getDoctors();
		const res =  await axios.post(req.url,{
		userName:this.state.username,password:this.state.password,roles:"ROLE_DOCTOR",name:this.state.name,experience:this.state.experience,speciality:this.state.speciality
		});
		console.log(res.data);
		this.setState({values:res.data,checkregister:true});
}

render(){
	return(
	<div className="RegisterforDoctor">
	
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<h1>Register Doctor</h1>
	<label style={{color:''}}>Username : </label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofusername}/><br /><br />
	<label style={{color:''}}>Name : </label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofname}/><br /><br />
	<label style={{color:''}}>Password : </label>
	<input type="password" class="form-control" placeholder="Password..." onChange={this.changeofpassword} /><br /><br />
	<label style={{color:''}}>Experience : </label>
	<input type="number" class="form-control" placeholder="Experience..." onChange={this.changeofexperience} /><br /><br />
	<label style={{color:''}}>Speciality : </label>
	<input type="text" class="form-control" placeholder="Speciality..." onChange={this.changeofspeciality} /><br /><br />
	<button type="submit" class="btn btn-primary" style={{textAlign:'center'}} onClick={this.submittingthedetailes}>Register</button>
	</form>
	{this.state.checkregister ? <div>{this.state.values ? <div>Doctor registered successfully</div>: <div style={{color:'red'}}>UserName already exists</div>}</div> : <div></div>}
	</div>
	)
	}
	}

export default RegisterforDoctor;