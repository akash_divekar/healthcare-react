import { Button } from 'antd';
import React from 'react';
import axios from 'axios';
import Urls from '../../Urls/Urls';


import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import UserProfile from '../../UserProfile';

class Availability extends React.Component {
    state = {hospitals:[], hosid:'', gen:'', list:[],status:false,
    d1:"Monday",d2:"Tuesday",d3:"Wednesday",d4:"Thursday",d5:"Friday",d6:"Saturday",d7:"Sunday",
    t1:'',t2:'',t3:'',t4:'',t5:'',t6:'',t7:'',
    tnop1:'',tnop2:'',tnop3:'',tnop4:'',tnop5:'',tnop6:'',tnop7:'',
    }

    componentDidMount = async() =>{
	const req = Urls.gettinghospitals();
	const res =  await axios.get(req.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
	this.setState({hospitals:res.data});
	console.log(this.state.hospitals);
	}

    changinghid = (e) =>{
	this.setState({hosid:e.target.value});
    }

    changeofgen = event =>{
        this.setState({gen:event.target.value})
    }

    time1 = event =>{
        this.setState({t1:event.target.value})
    }

    time2 = event =>{
        this.setState({t2:event.target.value})
    }

    time3 = event =>{
        this.setState({t3:event.target.value})
    }

    time4 = event =>{
        this.setState({t4:event.target.value})
    }

    time5 = event =>{
        this.setState({t5:event.target.value})
    }

    time6 = event =>{
        this.setState({t6:event.target.value})
    }

    time7 = event =>{
        this.setState({t7:event.target.value})
    }


    nop1 = event =>{
        this.setState({tnop1:event.target.value})
    }

    nop2 = event =>{
        this.setState({tnop2:event.target.value})
    }

    nop3 = event =>{
        this.setState({tnop3:event.target.value})
    }

    nop4 = event =>{
        this.setState({tnop4:event.target.value})
    }

    nop5 = event =>{
        this.setState({tnop5:event.target.value})
    }

    nop6 = event =>{
        this.setState({tnop6:event.target.value})
    }

    nop7 = event =>{
        this.setState({tnop7:event.target.value})
    }


    adding =() =>{
        this.state.list.push({day:this.state.d1,time:this.state.t1,nop:this.state.tnop1});
        this.state.list.push({day:this.state.d2,time:this.state.t2,nop:this.state.tnop2});
        this.state.list.push({day:this.state.d3,time:this.state.t3,nop:this.state.tnop3});
        this.state.list.push({day:this.state.d4,time:this.state.t4,nop:this.state.tnop4});
        this.state.list.push({day:this.state.d5,time:this.state.t5,nop:this.state.tnop5});
        this.state.list.push({day:this.state.d6,time:this.state.t6,nop:this.state.tnop6});
        this.state.list.push({day:this.state.d7,time:this.state.t7,nop:this.state.tnop7});

        console.log(this.state.list);
    }

    submittingthedetailes = async(event) =>{
        event.preventDefault();
        this.adding();
        const req = Urls.availability();
		const res =  await axios.post(req.url,{hid:this.state.hosid,sch:this.state.list,gen:this.state.gen},
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        this.setState({status:res.data});
        console.log(res.data);
        this.setState({list:[]});
        this.setState({t1:'',t2:'',t3:'',t4:'',t5:'',t6:'',t7:''});
        this.setState({tnop1:'',tnop2:'',tnop3:'',tnop4:'',tnop5:'',tnop6:'',tnop7:''})
    }

    render() {
        return (
            <div className="Availability">
            <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.submittingthedetailes}>

            <label style={{color:''}}>Hospital : </label>
            <select class="form-control" value={this.state.hosid} onChange={this.changinghid}>
	            <option>Select Hospital</option>
	             {this.state.hospitals.map((i)=>(<option class="form-control" value={i.id}>{i.name}</option>))}
	        </select><br />

            <TableHead>
            <TableRow>
            <TableCell align="right">Day</TableCell> 
            <TableCell align="right">Time</TableCell>
            <TableCell align="right">Total No. Of Patients</TableCell>
            </TableRow>
            </TableHead>

            <TableBody>
            <TableRow>
            <TableCell align="right">Monday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t1} onChange={this.time1}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop1} onChange={this.nop1} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Tuesday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t2} onChange={this.time2}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop2} onChange={this.nop2} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Wednesday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t3} onChange={this.time3}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop3} onChange={this.nop3} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Thursday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t4} onChange={this.time4}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop4} onChange={this.nop4} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Friday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t5} onChange={this.time5}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop5} onChange={this.nop5} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Saturday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t6} onChange={this.time6}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop6} onChange={this.nop6} min='1'/></TableCell>
            </TableRow>

            <TableRow>
            <TableCell align="right">Sunday</TableCell> 
            <TableCell align="right"><input class="form-control" type="time" value={this.state.t7} onChange={this.time7}/></TableCell>
            <TableCell align="right"><input class="form-control" type="number" value={this.state.tnop7} onChange={this.nop7} min='1'/></TableCell>
            </TableRow>
            </TableBody>

             <br /><br />
             <label style={{color:''}}>Enter the no of days for which the availability should be generated for the above schedule : </label>
             <input class="form-control" style={{width:'50%'}} type="number" onChange={this.changeofgen} min='1'/><br /><br />

			<button class="btn btn-primary" type="submit">Submit</button><br />
       
			</form>
            {this.state.status ? <div>Availability generated successfully</div> : <div></div>}
            </div>
        )
    }

}

export default Availability;