import React from 'react';
import './AddSession.css';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserProfile from '../../UserProfile';

class AddSession extends React.Component {

    state = {
        seats : null,
        topic : '',
        date : null,
        time : null,
        session : null,
    }

    componentDidMount = () => {
        
    }

    addSession = async(e) => {
        e.preventDefault();
        console.log("hid" + this.state.selectedHospitalId);
        console.log("seats" + this.state.seats);
        console.log("date" + this.state.date);
        console.log("topic" + this.state.topic);
        console.log("time" + this.state.time);
        const request = AllApi.addSessionToHospital();
        const response = await Axios.post(request.url, 
            {totSeats:this.state.seats,topic:this.state.topic,date:this.state.date,time:this.state.time}, 
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        console.log(response.data);
        this.setState({session:response.data});
    }

    setTotalSeats = (e) => {
        this.setState({seats:e.target.value});
    }

    setTopic = (e) => {
        this.setState({topic:e.target.value});
    }

    setDate =(e) => {
        this.setState({date:e.target.value});
    }
    
    setTime = (e) => {
        this.setState({time:e.target.value});
    }

    render() {

        const { seats,topic,date,time } = this.state;

        return (
            <div className="addSession-wrapper">
                <form style={{"width":"50%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.addSession}>
                    <h2 style={{"textAlign":"center"}}>Add Session</h2>
                    <div class="form-group">
                        <label>Total Number of Seats</label>
                        <input type="number" class="form-control" value={seats} placeholder="Total Seats" onChange={this.setTotalSeats} min="1" required/>
                    </div>
                    <div class="form-group">
                        <label>Topic</label>
                        <input type="text" class="form-control" value={topic} placeholder="Topic" onChange={this.setTopic} required/>
                    </div>
                    <div class="form-group">
                        <label>Select Date</label>
                        <input type="date" class="form-control" value={date} onChange={this.setDate} required/>
                    </div>
                    <div class="form-group">
                        <label>Select Time</label>
                        <input type="time" class="form-control" value={time} onChange={this.setTime} required/>
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
                <br/>
                {
                    this.state.session ? <div><b>Session Added Successfully</b></div> : <div></div>
                }
                <br/><br/>
            </div>
        )
    }

}

export default AddSession;