import React from 'react';
import axios from 'axios';
import './SystemAdmin.css';
import Urls from '../../Urls/Urls';

import UserProfile from '../../UserProfile';

class RemoveHospital extends React.Component{

state={hospitals:[],hid:null,check:null}

componentDidMount = async() =>{
	this.fetch();
	}

fetch = async() =>{
	const req = Urls.getHospitalsList();
	const res =  await axios.get(req.url,
		{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
	this.setState({hospitals:res.data});
	console.log(this.state.hospitals);
	}

changinghid = (e) =>{
	this.setState({hid:e.target.value});
}

submittingthedetailes = async(event) =>{
		event.preventDefault();
		const req = Urls.deletehospital();
		const res =  await axios.delete(`${req.url}/${this.state.hid}`,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
		console.log(res.data);
		this.setState({check:true})
		this.fetch();
}

render(){
	return(
	<div className="RemoveHospital">
	<h1>Remove Hospital</h1>
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<label style={{color:''}}>Enter Hospital Id :</label>

	<select class="form-control" value={this.state.hid} onChange={this.changinghid}>
	<option>Select Hospital</option>
	{this.state.hospitals.map((i)=>(
	<option class="form-control" value={i.id}>{i.name}</option>
	))}
	</select>

	<br /><br />
	<button type="submit" class="btn btn-primary" onClick={this.submittingthedetailes}>Remove</button>
	</form>
	{this.state.check ?<div>Removed hospital successfully</div>:<div></div>}
	</div>
	)
	}
	}

export default RemoveHospital;