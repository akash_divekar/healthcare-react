import { Button } from 'antd';
import React from 'react';
import axios from 'axios';
import Urls from '../../Urls/Urls';
import UserProfile from '../../UserProfile';

import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";

class Appointments extends React.Component {
    state = { 
        appointments : [], 
        show : true , 
        val : '', 
        prescription : '', 
        comments : '', 
        pname : '', 
        page : '',
        profiles: [],
        sharedValue: null,
        secondOpinion: null,
        profileId: null,
        profile: null,
    }

    componentDidMount = async()=>{
        this.fetch();
    }

    fetch = async()=>{
        const req = Urls.getAppointments();
		const res =  await axios.get(req.url,
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,'id':UserProfile.getId()}});
        this.setState({appointments:res.data});
		console.log(res.data);
    }

    movetoprescription = (id,name,age,share,sop,ppid) =>{
    this.setState({show : false, val : id, pname : name, page : age,sharedValue:share, secondOpinion:sop, profileId:ppid})
    }

    changeofprescription = event =>{
        this.setState({prescription:event.target.value})
    }

    changeofcomments = event =>{
        this.setState({comments:event.target.value})
    }

    movetoappointments =async(event)=>{
        event.preventDefault();
		const req = Urls.addToPatProfile();
		const res =  await axios.post(req.url,{apid:this.state.val,prescription:this.state.prescription,comments:this.state.comments},
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
        console.log(res.data);
        const request = Urls.appointment();
		const response =  await axios.patch(`${request.url}/${this.state.val}`,{},{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
        console.log(response.data);
        this.setState({show:true});
        this.fetch();
    }

    viewPatientProfile = async() => {
        const req = Urls.profiles();
        const res = await axios.get(req.url, {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`},
            params : {apid:this.state.val}});
        this.setState({profiles : res.data});
    }

    displayPatientProfiles = () => (
        this.state.profiles.map(profile=>{
            return(
                <TableRow key={profile.id}>
                <TableCell>{profile.date}</TableCell>
                <TableCell>{profile.doctor.name}</TableCell>
                <TableCell>{profile.prescription}</TableCell>
                <TableCell>{profile.comments}</TableCell>
                </TableRow>
            )
        })
    )

    fetchPatientProfile = async() => {
        console.log("entry");
        const req = Urls.profile();
        const res = await axios.get(req.url, {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`},
            params : {apid:this.state.val}});
        console.log(res.data);
        this.setState({profile:res.data});
        console.log(this.state.profile);
    }

    render() {

        return (
        <div>
        {this.state.show ?
            <div className="Appointments">
                <b style={{fontSize:"20px"}}>Appointments</b><br/><br/>
            <table style={{width:'100%'}}>
            <TableHead>
            <TableRow>
            <TableCell>Patient Name</TableCell> 
            <TableCell>Age</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Time</TableCell>
            <TableCell>Hospital Name</TableCell> 
            <TableCell>Location</TableCell> 
            <TableCell>Attend</TableCell> 
            <TableCell>Second Opinion</TableCell> 
            </TableRow>
            </TableHead>

            {this.state.appointments.map((appointment)=>(
            <TableBody>
            <TableRow>
            <TableCell>{appointment.patient.name}</TableCell>
            <TableCell>{appointment.patient.age}</TableCell>
            <TableCell>{appointment.date}</TableCell>
            <TableCell>{appointment.time}</TableCell>
            <TableCell>{appointment.hospital.name}</TableCell>
            <TableCell>{appointment.hospital.area},{appointment.hospital.city},{appointment.hospital.state}</TableCell>
            <TableCell>
            {!appointment.done?
            <button class="btn btn-primary" onClick={()=>this.movetoprescription(appointment.id,appointment.patient.name,appointment.patient.age,appointment.share,appointment.sop,appointment.ppid)}>Attend</button>
            :
            <div>Attended</div>
            }
            </TableCell>
            <TableCell>{appointment.sop ? <div>Yes</div> : <div>No</div>}</TableCell>
            </TableRow>
            </TableBody>
            ))}

            </table>
            </div>

            :

            <div className="AddPatientProfile">
            <h1>Prescription</h1>
            <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
            <label style={{color:''}}>Patient Name : {this.state.pname}</label><br /><br />
            <label style={{color:''}}>Age : {this.state.page}</label><br /><br />
			<label style={{color:''}}>Prescription : </label>
			<textarea class="form-control" placeholder="Prescription..." onChange={this.changeofprescription}/><br /><br />
            <label style={{color:''}}>Comments : </label>
			<textarea class="form-control" placeholder="Comments..." onChange={this.changeofcomments}/><br /><br />
			<button type="submit" class="btn btn-primary" onClick={this.movetoappointments}>Submit</button>
			</form><br/>
            {
                this.state.secondOpinion ?<div>Please click the below button to view the patient's profile for which second opinion was requested.<br/>
                <Button type="primary" onClick={this.fetchPatientProfile}>View</Button></div> : <div></div>
            }
            <br/><br/>
            {
                (this.state.secondOpinion && this.state.profile) ? 
                <TableContainer component={Paper}>
                    <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Doctor Name</TableCell>
                        <TableCell>Prescription</TableCell>
                        <TableCell>Comments</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody><TableCell>{this.state.profile.date}</TableCell>
                <TableCell>{this.state.profile.doctor.name}</TableCell>
                <TableCell>{this.state.profile.prescription}</TableCell>
                <TableCell>{this.state.profile.comments}</TableCell></TableBody></Table>
                </TableContainer>
                 : <div></div>
            }<br/><br/>
            {
                this.state.sharedValue ? <Button type="primary" onClick={this.viewPatientProfile}>View Profiles</Button> : <div></div>
            }
            <br/><br/>
            {
                (this.state.profiles.length && this.state.sharedValue)  ? <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Doctor Name</TableCell>
                        <TableCell>Prescription</TableCell>
                        <TableCell>Comments</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{this.displayPatientProfiles()}</TableBody></Table>
            </TableContainer> : <div></div>
            }
            </div>
            }
            </div>
        )
    }

}

export default Appointments;