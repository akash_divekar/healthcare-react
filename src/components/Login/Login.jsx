import React from 'react';
import { Button } from 'antd';
import axios from 'axios';
import Urls from '../../Urls/Urls';
import UserProfile from '../../UserProfile';
import Patient from '../patient/Patient';
import HospitalAdmin from '../hospital admin/HospitalAdmin';
import {
    BrowserRouter as Router,
    Switch,
    Route,
	Link,
	Redirect
    } from "react-router-dom";

class Login extends React.Component{

			state = {
				credentials:[],
				username:'',
				password:'',
				checkUser:true
			}

			changeofusername = event =>{
				this.setState({username:event.target.value});
			}

			changeofpassword = event =>{
				this.setState({password:event.target.value});
			}

			loggingdetailes = async(event) =>{
			event.preventDefault();
			var base64 = require('base-64');
			var utf8 = require('utf8');

			var username = this.state.username;
			var password = this.state.password;
			var text = username+':'+password;
			var bytes = utf8.encode(text);
			var encoded = base64.encode(bytes);
			console.log(encoded);
			const req = Urls.login();
			this.setState({checkUser:true});
			const res =  await axios.post(req.url,{},{headers:{'Authorization':`Basic ${encoded}`}})
			.catch(err =>  {
				if(err.response) {
					this.setState({checkUser:false});
				}
			});
			console.log(this.state.checkUser);
			if(this.state.checkUser) {
			this.setState({credentials:res.data});
			console.log("crd",this.state.credentials);
			UserProfile.setEncodedValue(encoded);
			UserProfile.setName(this.state.credentials.username);
			UserProfile.setRole(this.state.credentials.role);
			UserProfile.setId(this.state.credentials.id);
			var u = "";
			if(this.state.credentials.role === "ROLE_PATIENT") {
				console.log(UserProfile.getName()+UserProfile.getRole()+UserProfile.getId()+UserProfile.getEncodedValue());
				u = "/patientPage";
			}
			else if(this.state.credentials.role === "ROLE_DOCTOR") {
				console.log(UserProfile.getName()+UserProfile.getRole()+UserProfile.getId());
				u = "/doctorpage";
			}
			else if(this.state.credentials.role === "ROLE_HOSADMIN") {
				console.log(UserProfile.getName()+UserProfile.getRole()+UserProfile.getId());
				u = "/hospitalAdminPage";
			}
			else if(this.state.credentials.role === "ROLE_SYSADMIN") {
				console.log(UserProfile.getName()+UserProfile.getRole()+UserProfile.getId());
				u = "/systemadmin";
			}
			this.props.history.push(u);
			}
			}

			back = () =>{
				this.props.history.push("/");
			}

			render(){
				return(
				<div className="Login" style={{margin:'1%',padding:'1%'}}>
				<Button onClick={this.back}>Back</Button><br /><br />
				<h1>Login</h1>
				<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.loggingdetailes}>
				<label style={{color:''}} >User Name : </label>
				<input type="text" class="form-control" placeholder="User Name..." onChange={this.changeofusername} /><br /><br />
				<label style={{color:''}} >Password : </label>
				<input type="Password" class="form-control" placeholder="Password..."  onChange={this.changeofpassword} /><br /><br />
				<button type="submit" class="btn btn-primary">Login</button>
				</form><br/>
				{
					this.state.checkUser ? <div></div> : <div style={{color:'red', marginLeft:'10px'}}>Invalid Credentials</div>
				}
				</div>
	)
	}
	}

export default Login;