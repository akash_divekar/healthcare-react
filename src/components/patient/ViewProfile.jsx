import React from 'react';
import './ViewProfile.css';
import AllApi from '../../api/AllApi';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Axios from 'axios'
import UserProfile from '../../UserProfile';
import { Button } from 'antd';
import SearchDoctor from './SearchDoctor';

class ViewProfile extends React.Component {

    state = {
        profiles : [],
        selectedShareValue : false,
        secondOpinion : false,
        patientProfileId: null,
    }

    componentDidMount = () => {
        this.getAllProfiles();
    }

    getAllProfiles = async() => {
        const request = AllApi.fetchPatientsAllProfiles();
        const response = await Axios.get(request.url,
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,
            'id':UserProfile.getId()}});
        this.setState({profiles : response.data});
        console.log(UserProfile.getId());
    }

    askSecondOpinion = (profileId) => {
        this.setState({patientProfileId:profileId});
        this.setState({secondOpinion:true});
    }

    displayAllProfiles = (allProfiles) => (
        allProfiles.map(profile=>{
            return(
                <TableRow key={profile.id}>
                <TableCell>{profile.date}</TableCell>
                <TableCell>{profile.doctor.name}</TableCell>
                <TableCell>{profile.prescription}</TableCell>
                <TableCell>{profile.comments}</TableCell>
                <TableCell><Button type="primary" onClick={() => this.askSecondOpinion(profile.id)} >Second Opinion</Button></TableCell>
                </TableRow>
            )
        })
    )

    selectShareValue = (event) => {
        this.setState({selectedShareValue :event.target.value});
    }

    render() {

        const { profiles } = this.state;

        return (
            <div className="viewProfile-wrapper">
                <h1>Profiles</h1>
                <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Doctor Name</TableCell>
                        <TableCell>Prescription</TableCell>
                        <TableCell>Comments</TableCell>
                        <TableCell>Second Opinion</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {profiles.length ? this.displayAllProfiles(profiles) : <TableRow><TableCell align="right">No Profiles</TableCell></TableRow>}
                </TableBody>
                </Table>
            </TableContainer><br/><br/><br/>
            {
                this.state.secondOpinion ? <SearchDoctor profileId={this.state.patientProfileId} secondOpinionValue={this.state.secondOpinion}></SearchDoctor> : <div></div>
            }
            </div>
        )
    }

}

export default ViewProfile;