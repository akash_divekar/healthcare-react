const patientBaseUrl = 'http://localhost:8081/patient';
const hospitalAdminBaseUrl = 'http://localhost:8081/hadmin';
const BaseUrl = 'http://localhost:8081';

const fetchDoctorsBySpeciality = () => ({
    url: `${patientBaseUrl}/doctorsbyspec`,
})

const fetchDoctorsByLocation = () => ({
    url: `${patientBaseUrl}/doctorsbyloc`,
})

const fetchPatientAppointments = () => ({
    url: `${patientBaseUrl}/patappointment`,
})

const fetchDoctorAvailability = () => ({
    url: `${patientBaseUrl}/doctorsavailibility`,
})

const bookDoctorAppointment = () => ({
    url: `${patientBaseUrl}/appointment`,
})

const fetchPatientsAllProfiles = () => ({
    url: `${patientBaseUrl}/patientprofiles`,
})

const patientCancelsAppointment = () => ({
    url: `${patientBaseUrl}/appointment`,
})

const reviewDoctor = () => ({
    url: `${patientBaseUrl}/reviewdoc`,
})

const reviewHospital = () => ({
    url: `${patientBaseUrl}/reviewhos`,
})

const fetchAllDoctors = () => ({
    url: `${hospitalAdminBaseUrl}/doctors`,
})

const fetchAllDoctorsNotInHospital = () => ({
    url: `${hospitalAdminBaseUrl}/doctorsnohos`,
})

const fetchDoctorsFromHospital = () => ({
    url: `${hospitalAdminBaseUrl}/doctorsbyhos`,
})

const fetchAllHospitals = () => ({
    url: `${hospitalAdminBaseUrl}/hospitals`,
})

const addDoctorToHospital = () => ({
    url: `${hospitalAdminBaseUrl}/adddoctor`,
})

const removeDoctorFromHospital = () => ({
    url: `${hospitalAdminBaseUrl}/rmdoctor`,
})

const addSessionToHospital = () => ({
    url: `${hospitalAdminBaseUrl}/addsessions`,
})

const fetchHospitalReviews = () => ({
    url: `${hospitalAdminBaseUrl}/reviews`,
})

const fetchSessions = () => ({
    url: `${patientBaseUrl}/sessions`,
})

const fetchMySessions = () => ({
    url: `${patientBaseUrl}/mysessions`,
})

const logout = () => ({
    url: `${BaseUrl}/logout`,
})

export default {
    fetchDoctorsBySpeciality,fetchDoctorsByLocation,fetchPatientAppointments,
    fetchDoctorAvailability,bookDoctorAppointment,fetchPatientsAllProfiles,
    patientCancelsAppointment,fetchAllDoctors,fetchAllHospitals,addDoctorToHospital,
    removeDoctorFromHospital,addSessionToHospital,logout,fetchSessions,fetchMySessions,
    fetchDoctorsFromHospital,fetchAllDoctorsNotInHospital,reviewDoctor,reviewHospital,
    fetchHospitalReviews
}