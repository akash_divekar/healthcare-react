import React from 'react';
import { Button } from 'antd';
import './Welcome.css'
import { Link } from "react-router-dom";

class Welcome extends React.Component{

render(){
	return(

	<div className="Welcome-Wrapper">
	<h1>Welcome</h1>
	<div className="WelcomeHome">
	<Link to="/login"><Button className="welcome-button">Login</Button></Link>
	<Link to="/register"><Button className="welcome-button">Register</Button></Link>
	</div>
	</div>
	);
}
}
export default Welcome;