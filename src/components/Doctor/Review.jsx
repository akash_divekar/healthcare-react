import { Button } from 'antd';
import React from 'react';
import axios from 'axios';
import Urls from '../../Urls/Urls';


import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import UserProfile from '../../UserProfile';

class Review extends React.Component {
	
	state = {viewreviews:[],}

	componentDidMount = async()=>{
        this.fetch();
    }

    fetch = async()=>{
        const req = Urls.getreviews();
		const res =  await axios.get(req.url,
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,'id':UserProfile.getId()}});
        this.setState({viewreviews:res.data});
		console.log(this.state.viewreviews);
    }

	render(){
	return (
		<div className="Review">
		<table style={{width:'100%'}}>
            <TableHead>
            <TableRow>
            <TableCell>Patient Name</TableCell>
            <TableCell>Rating</TableCell>
            <TableCell>Review</TableCell>
			</TableRow>
            </TableHead>

			{this.state.viewreviews.map((r)=>(
			<TableBody>
            <TableRow>
			<TableCell>{r.patient.name}</TableCell>
            <TableCell>{r.rating}</TableCell>
            <TableCell>{r.review}</TableCell>
			</TableRow>
            </TableBody>
			))}
		</table>
		</div>
	)
	}
}

export default Review;