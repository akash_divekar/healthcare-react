import React from 'react';
import './RemoveDoctorFromHospital.css';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserProfile from '../../UserProfile';

class RemoveDoctorFromHospital extends React.Component {

    state = {
        allDoctors : [],
        selectedDoctorId : null,
        hospital : null
    }

    componentDidMount = () => {
        this.getAllDoctors();
    }

    getAllDoctors = async() => {
        const request = AllApi.fetchDoctorsFromHospital();
        const response = await Axios.get(request.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        this.setState({allDoctors : response.data});
        console.log(this.state.allDoctors);
    }

    selectDoctorId = (e) =>{
        this.setState({selectedDoctorId:e.target.value});
    }

    removeDoctorFromHospital = async(event) => {
        event.preventDefault();
        console.log(this.state.selectedDoctorId);
        console.log(this.state.selectedHospitalId);
        const request = AllApi.removeDoctorFromHospital();
        const response = await Axios.delete(request.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()},
        data : {"did":this.state.selectedDoctorId}});
        this.setState({hospital:response.data});
        this.getAllDoctors();
        console.log(response.data);
    }

    render() {

        const { selectedDoctorId, allDoctors} = this.state;

        return (
            <div className="removeDoctor-wrapper">
                <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
                <h2 style={{"textAlign":"center"}}>Remove Doctor From Hospital</h2>
                <div class="form-group">
                    <label>Select Doctor : </label>
                    <select class="form-control" value={selectedDoctorId} onChange={this.selectDoctorId}>
                    <option>Select Doctor</option>
                        {allDoctors.map((doctor)=>(
                            <option value={doctor.id}>{doctor.name}</option>
                        ))}
                    </select>
                    </div>
                    <button type="submit" class="btn btn-primary" onClick={this.removeDoctorFromHospital} >Remove</button>
                </form>
                {
                    this.state.hospital ? <div><b>Docter Removed Successfully</b></div> : <div></div>
                }
            </div>
        )
    }
}

export default RemoveDoctorFromHospital;