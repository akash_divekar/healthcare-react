import React from 'react';
import axios from 'axios';
import './Styles.css';
import Urls from '../../Urls/Urls';
import { Link } from "react-router-dom";

class RegisterforPatient extends React.Component{

state={values:null,username:'',name:'',password:'',age:'',checkregister:false}

changeofusername = event =>{
	this.setState({username:event.target.value});
}

changeofname = event =>{
	this.setState({name:event.target.value});
}

changeofpassword = event =>{
	this.setState({password:event.target.value});
}

changeofage = event =>{
	this.setState({age:event.target.value});
}

submittingthedetailes = async(event) =>{
		event.preventDefault();
		const req = Urls.getPatients();
		const res =  await axios.post(req.url,{
		userName:this.state.username,password:this.state.password,roles:"ROLE_PATIENT",name:this.state.name,age:this.state.age
		});
		this.setState({values:res.data,checkregister:true});
		console.log(res.data);
}

render(){
	return(
	<div className="RegisterforPatient">
	
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<h1>Register Patient</h1>
	<label style={{color:''}}>Username :</label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofusername}/><br /><br />
	<label style={{color:''}}>Name :</label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofname}/><br /><br />
	<label style={{color:''}}>Password : </label>
	<input type="password" class="form-control" placeholder="Password..." onChange={this.changeofpassword} /><br /><br />
	<label style={{color:''}}>Age : </label>
	<input type="number" class="form-control" placeholder="Age..." onChange={this.changeofage} /><br /><br />
	<button class="btn btn-primary" style={{textAlign:'center'}} onClick={this.submittingthedetailes}>Register</button>
	</form>
	{this.state.checkregister ? <div>{this.state.values ? <div>Patient registered successfully</div>: <div style={{color:'red'}}>UserName already exists</div>}</div> : <div></div>}
	</div>
	)
	}
	}

export default RegisterforPatient;