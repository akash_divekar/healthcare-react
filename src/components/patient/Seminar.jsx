import { Button } from 'antd';
import React from 'react';
import AllApi from '../../api/AllApi';
import 'bootstrap/dist/css/bootstrap.min.css';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import axios from 'axios'
import UserProfile from '../../UserProfile';

class Seminar extends React.Component{
  state={
    sessions:[],
    mysessions:[],
  }
  
  componentDidMount()
    {
       this.fetch();
    }

    fetch =async()=>{
        const request = AllApi.fetchSessions();
        const request1 = AllApi.fetchMySessions();
        const response = await axios.get(request.url,
          {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        const response1 = await axios.get(request1.url,
          {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        console.log(response.data);
        console.log(response1.data);
        this.setState({sessions:response.data});
        this.setState({mysessions:response1.data});
    }
  rendersessions=(sessions)=>(
    sessions.map(s=>{
      return(        
                <TableRow key={s.id}>
                <TableCell>{s.date}</TableCell>
                <TableCell>{s.time}</TableCell>
                <TableCell>{s.topic}</TableCell>
                <TableCell>{s.remSeats}</TableCell>
                <TableCell>{s.hospital.name}</TableCell>
                <TableCell>{s.hospital.area},{s.hospital.city},{s.hospital.state}</TableCell>
                <TableCell><Button type="primary" onClick={() =>this.book(s.id)}>BOOK</Button></TableCell>
                </TableRow>
            )
            
        }
        
      )
    )

    renderMysessions=(mysessions)=>(
        mysessions.map(s=>{
          return(        
                    <TableRow key={s.id}>
                    <TableCell>{s.date}</TableCell>
                    <TableCell>{s.time}</TableCell>
                    <TableCell>{s.topic}</TableCell>
                    <TableCell>{s.hospital.name}</TableCell>
                    <TableCell>{s.hospital.area},{s.hospital.city},{s.hospital.state}</TableCell>
                    </TableRow>
                )
                
            }
            
          )
        )

    book=async(s)=>
    {
        const request = AllApi.fetchSessions();
        const response = await axios.post(`${request.url}/${s}`,null,
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        console.log(response.data);
        this.fetch();
        
    }

  
  render() {
    const {sessions,mysessions}=this.state;
    return (
      
        <div >
            <h1>Sessions</h1>
            <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Time</TableCell>
                        <TableCell>Topic</TableCell>
                        <TableCell>Remaining Seats</TableCell>
                        <TableCell>Hospital</TableCell>
                        <TableCell>Location</TableCell>
                        <TableCell>Book Session</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {sessions.length ? this.rendersessions(sessions) : <TableRow><TableCell align="right">No New Sessions</TableCell></TableRow>}
                    </TableBody>
                </Table>
            </TableContainer><br/><br/><br/>
            <h1>My Sessions</h1>
            <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        
                        <TableCell>Date</TableCell>
                        <TableCell>Time</TableCell>
                        <TableCell>Topic</TableCell>
                        <TableCell>Hospital</TableCell>
                        <TableCell>Location</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {mysessions.length ? this.renderMysessions(mysessions) : <TableRow><TableCell align="right">No Sessions Booked</TableCell></TableRow>}
                    </TableBody>
                </Table><br/><br/><br/>
            </TableContainer>         
        </div>
);  
  }
}

export default Seminar;
