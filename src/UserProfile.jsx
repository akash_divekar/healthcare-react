var UserProfile = (function() {
    var full_name = "";
    var id = null;
    var role = "";
    var encodedValue = null;
  
    var getName = function() {
      return full_name;   
    };
  
    var setName = function(name) {
      full_name = name;     
    };

    var getId = function() {
      return id;   
    };
  
    var setId = function(fetchedId) {
      id = fetchedId;     
    };

    var getRole = function() {
      return role ; 
    };
  
    var setRole = function(fetchedRole) {
      role = fetchedRole;     
    };

    var getEncodedValue = function() {
      return encodedValue ; 
    };
  
    var setEncodedValue = function(fetchedEncodedValue) {
      encodedValue = fetchedEncodedValue;     
    };
    
  
    return {
      getName: getName,
      setName: setName,
      getId: getId,
      setId: setId,
      getRole: getRole,
      setRole: setRole,
      getEncodedValue: getEncodedValue,
      setEncodedValue: setEncodedValue,
    }
  
  })();
  
  export default UserProfile;