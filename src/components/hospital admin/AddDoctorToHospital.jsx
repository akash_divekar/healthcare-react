import React from 'react';
import './AddDoctorToHospital.css';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import 'bootstrap/dist/css/bootstrap.min.css';
import UserProfile from '../../UserProfile';

class AddDoctorToHospital extends React.Component {

    state = {
        allDoctors : [],
        selectedDoctorId : null,
        hospital : null,
        check:false,
    }

    componentDidMount = () => {
        this.setState({hospital:null})
        this.getAllDoctors();
    }

    getAllDoctors = async() => {
        const request = AllApi.fetchAllDoctorsNotInHospital();
        const response = await Axios.get(request.url,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        this.setState({allDoctors : response.data});
        console.log(this.state.allDoctors);
    }

    selectDoctorId = (e) =>{
        this.setState({selectedDoctorId:e.target.value});
    }

    insertDoctorToHospital = async(event) => {
        event.preventDefault();
        console.log(this.state.selectedDoctorId);
        this.setState({check:true});
        console.log(this.state.check);
        const request = AllApi.addDoctorToHospital();
        const response = await Axios.post(request.url, {"did":this.state.selectedDoctorId},
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
        this.setState({hospital:response.data});
        this.getAllDoctors();
        console.log(response.data);
    }

    render() {

        const { selectedDoctorId, allDoctors} = this.state;

        return (
            <div className="addDoctor-wrapper">
                <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
                <h2 style={{"textAlign":"center"}}>Add Doctor to Hospital</h2>
                <div class="form-group">
                    <label>Select Doctor : </label>
                    <select class="form-control" value={selectedDoctorId} onChange={this.selectDoctorId}>
                    <option>Select Doctor</option>
                        {allDoctors.map((doctor)=>(
                            <option value={doctor.id}>{doctor.name}</option>
                        ))}
                    </select>
                    </div>
                    <button type="submit" class="btn btn-primary" onClick={this.insertDoctorToHospital} >Add</button>
                </form>
                {
                    this.state.hospital ? 
                    <div><b>Docter added successfully</b></div> 
                    : 
                    <div>
                    </div>
                }
            </div>
        )
    }

}

export default AddDoctorToHospital;