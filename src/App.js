import './App.css';
import Patient from './components/patient/Patient';
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HospitalAdmin from './components/hospital admin/HospitalAdmin';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
  } from "react-router-dom";
import Welcome from './components/Welcome/Welcome';
import Login from './components/Login/Login';
import RegisterHome from './components/Register/RegisterHome';
import RegisterforPatient from './components/Register/RegisterforPatient';
import RegisterforDoctor from './components/Register/RegisterforDoctor';
import RegisterforHospitaladmin from './components/Register/RegisterforHospitaladmin';
import DoctorPage from './components/Doctor/DoctorPage';
import Appointments from './components/Doctor/Appointments';
import AddHospital from './components/Systemadmin/AddHospital';
import RemoveHospital from './components/Systemadmin/RemoveHospital';
import AddHospitalAdmin from './components/Systemadmin/AddHospitalAdmin';
import RemoveHospitalAdmin from './components/Systemadmin/RemoveHospitalAdmin';
import SystemAdmin from './components/Systemadmin/SystemAdmin';
import SearchDoctor from './components/patient/SearchDoctor';
import DisplayAppointments from './components/patient/DisplayAppointments';
import ViewProfile from './components/patient/ViewProfile';
import UserProfile from './UserProfile';
import React,{useState} from 'react';

function App() {
  return (
    <Router>
      <div className="App">
        {
            UserProfile.getId() ? null : <Redirect to="/" />
        }
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={RegisterHome} />
          <Route exact path="/doctorpage" component={DoctorPage} />
          <Route exact path="/appointments" component={Appointments} />
          <Route exact path="/systemadmin" component={SystemAdmin} />
          <Route exact path="/patientPage" component={Patient} />  
          <Route exact path="/hospitalAdminPage" component={HospitalAdmin} />
        </Switch>
      </div>
      </Router>
    );
}

export default App;
