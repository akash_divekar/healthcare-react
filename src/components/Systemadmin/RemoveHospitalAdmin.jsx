import React from 'react';
import axios from 'axios';
import './SystemAdmin.css';
import Urls from '../../Urls/Urls';

import UserProfile from '../../UserProfile';

class RemoveHospitalAdmin extends React.Component{
	

state={hospitaladmins:[],haid:null,check:null}

componentDidMount = async() =>{
	this.fetch();
	}

fetch = async() =>{
	const req = Urls.gethospitaladminforsys();
	const res =  await axios.get(req.url,
		{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,id:UserProfile.getId()}});
	this.setState({hospitaladmins:res.data});
	console.log(this.state.hospitaladmins);
	}

changinghaid = (e) =>{
	this.setState({haid:e.target.value});
}

submittingthedetailes = async(event) =>{
		event.preventDefault();
		const req = Urls.deletehospitaladmin();
		const res =  await axios.delete(`${req.url}/${this.state.haid}`,{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
		console.log(res.data);
		this.setState({check:true});
		this.fetch();
}

render(){
	return(
	<div className="RemoveHospitalA">
	<h1>Remove Hospital Admin</h1>
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<label style={{color:''}}>Select Hospital Admin :</label>

	<select class="form-control" value={this.state.haid} onChange={this.changinghaid}>
	<option>Select Hospital Admin</option>
	{this.state.hospitaladmins.map((i)=>(
	<option class="form-control" value={i.id}>{i.name}</option>
	))}
	</select>
	<br /><br />
	<button type="submit" class="btn btn-primary" onClick={this.submittingthedetailes}>Remove Admin</button>
	</form>
	{this.state.check ?<div>Removed hospital admin successfully</div>:<div></div>}
	</div>
	)
	}
	}

export default RemoveHospitalAdmin;