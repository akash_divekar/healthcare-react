const basicUrl='http://localhost:8081';
const registerbasicUrl='http://localhost:8081/register';
const doctorbasicUrl='http://localhost:8081/doctor';
const systemadminbasicUrl='http://localhost:8081/sysadmin';

const login = () => ({
	url:`${basicUrl}/login`,
})

const getPatients = () => ({
	url:`${registerbasicUrl}/patients`,
})

const getDoctors = () => ({
	url:`${registerbasicUrl}/doctors`,
})

const getHospitaladmins = () => ({
	url:`${registerbasicUrl}/hosadmin`,
})

const getHospitalsList = () => ({
	url:`${systemadminbasicUrl}/hospitals`, 
})

const addnewhospital = () => ({
	url:`${systemadminbasicUrl}/hospitals`, 
})

const deletehospital = () => ({
	url : `${systemadminbasicUrl}/hospitals`,
})

const getHospitals = () => ({
	url:`${systemadminbasicUrl}/hospitalsbyadmin`,
})

const getHospitalAdmin = () => ({
	url:`${systemadminbasicUrl}/hosadminnohos`,
})

const gethospitaladminforsys = () => ({
	url:`${systemadminbasicUrl}/hosadminbyhos`,
})

const addhostohosadmin = () => ({
	url:`${systemadminbasicUrl}/addadmintohos`,
})

const deletehospitaladmin = () => ({
	url : `${systemadminbasicUrl}/rmadminfromhos`,
})


const getAppointments = () => ({
	url : `${doctorbasicUrl}/docappointment`,
})

const availability = () => ({
	url : `${doctorbasicUrl}/availability`,
})

const profiles = () => ({
	url : `${doctorbasicUrl}/patientprofiles`,
})

const profile = () => ({
	url : `${doctorbasicUrl}/patientprofile`,
})


const appointment = () => ({
	url : `${doctorbasicUrl}/appointment`,
})

const addToPatProfile = () => ({
	url : `${doctorbasicUrl}/patientprofile`,
})


const logoutprofile = () => ({
	url : `${basicUrl}/logout`,
})

const gettinghospitals = () => ({
	url : `${doctorbasicUrl}/dochospitals`,
})

const getreviews = () => ({
	url : `${doctorbasicUrl}/reviews`,
})

export default {login,getPatients,getDoctors,getHospitaladmins,
addnewhospital,getHospitals,getHospitalAdmin,addhostohosadmin,getHospitalsList,deletehospital,gethospitaladminforsys,deletehospitaladmin,
getAppointments,availability,profiles,profile,appointment,addToPatProfile,logoutprofile,gettinghospitals,getreviews
}