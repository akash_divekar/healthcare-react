import { Button } from 'antd';
import React from 'react';
import './SearchDoctor.css';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import UserProfile from '../../UserProfile';

class SearchDoctor extends React.Component {

    state = {
        speciality : '',
        area : '',
        city : '',
        astate : '',
        doctors: null,
        showForm : null,
        showDoctors: false,
        showDoctorAvailability: false,
        doctorAvailability : null,
        checkAppointment:false,
        selectedShareValue : false,
    }

    getDoctorsByLoc = async(event) => {
        event.preventDefault();
        this.setState({showForm:0});
        this.setState({showDoctorAvailability : false});
        this.setState({checkAppointment:false});
        const request = AllApi.fetchDoctorsByLocation();
        const response = await Axios.get(request.url, 
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`},
            params : {area:this.state.area, city:this.state.city, state:this.state.astate}});
        this.setState({doctors : response.data});
        this.setState({showDoctors : true});
    }

    getDoctorsBySpec = async(event) => {
        event.preventDefault();
        this.setState({showForm:0});
        this.setState({showDoctorAvailability : false});
        this.setState({checkAppointment:false});
        const request = AllApi.fetchDoctorsBySpeciality();
        const response = await Axios.get(request.url, {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`},
            params : {speciality:this.state.speciality}});
        this.setState({doctors : response.data});
        if(this.state.doctors.length == 0) {
            console.log("hi");
        }
            console.log(this.state.doctors)
        
        this.setState({showDoctors : true});
    }

    getDoctorAvailability = async(doctorId) => {
        console.log("docid",doctorId);
        this.setState({showDoctors : false});
        const request = AllApi.fetchDoctorAvailability();
        const response = await Axios.get(request.url, 
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,'id':UserProfile.getId()},
            params : {did:doctorId}});
        this.setState({doctorAvailability:response.data});
        this.setState({showDoctorAvailability : true});
        console.log(this.state.doctorAvailability);
    }

    bookAppointment = async(availabilityId) => {
        console.log("hello");
        const ppid = this.props.profileId;
        const sop = this.props.secondOpinionValue;
        console.log("avail",availabilityId);
        console.log("pid",ppid);
        console.log("second",sop);
        console.log("sharedval",this.state.selectedShareValue);
        const request = AllApi.bookDoctorAppointment();
        const response = await Axios.post(request.url, 
        {"avid":availabilityId,"share":this.state.selectedShareValue,"sop":sop,"ppid":ppid},
        {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,'id':UserProfile.getId()}});
        console.log("avail",response.data);
        this.setState({showDoctorAvailability : false});
        this.setState({showDoctors : false});
        this.setState({checkAppointment:true});
    }

    displayDoctorAvailability = (doctorAvailability) => (
        doctorAvailability.map(availability=>{
            return(
                <TableRow key={availability.id}>
                    <TableCell>{availability.date}</TableCell>
                    <TableCell>{availability.time}</TableCell>
                    <TableCell>{availability.hospital.area},{availability.hospital.city},{availability.hospital.state}</TableCell>
                    <TableCell><Button type="primary" onClick={() => this.bookAppointment(availability.id)}>Book</Button></TableCell>
                    <TableCell><select class="form-control" value={this.state.selectedShareValue} onChange={this.selectShareValue}>
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                    </select></TableCell>
                    </TableRow>
            ) 
        })
    )

    displayDoctors = (doctors) => (
        doctors.map(doctor=>{
            return(
                <TableRow key={doctor.id}>
                    <TableCell>{doctor.name}</TableCell>
                    <TableCell>{doctor.experience}</TableCell>
                    <TableCell>{doctor.speciality}</TableCell>
                    <TableCell><Button type="primary" onClick={() => this.getDoctorAvailability(doctor.id)}>Get</Button></TableCell>
                </TableRow>
            ) 
        })
    )

    handleArea = (event) => {
        this.setState({area: event.target.value})
    }

    handleCity = (event) => {
        this.setState({city: event.target.value})
    }

    handleAstate = (event) => {
        this.setState({astate: event.target.value})
    }

    handleSpeciality = (event) => {
        this.setState({speciality: event.target.value})
    }

    selectShareValue = (event) => {
        this.setState({selectedShareValue :event.target.value});
    }

    render() {
        const { doctors,showDoctors,showForm,doctorAvailability,showDoctorAvailability,checkAppointment } = this.state;
        return(
            <div className="searchDoctor-wrapper">
                <h1>SearchDoctor</h1>
                <Button className="searchDoctor-button" onClick={() => this.setState({showForm:1, showDoctors:false,showDoctorAvailability:false,checkAppointment:false})}>By Location</Button>
                <Button className="searchDoctor-button" onClick={() => this.setState({showForm:2, showDoctors:false,showDoctorAvailability:false,checkAppointment:false})}> By Speciality</Button>
                {(() => {
                    switch (showForm) {
                    case 1:   return(<div>
                                        <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.getDoctorsByLoc}>
                                        <h2 style={{"textAlign":"center"}}>Search By Location</h2>
                                        <div class="form-group">
                                            <label>Enter area : </label>
                                            <input class="form-control" type="text" value={this.state.area} onChange={this.handleArea} required/>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter city : </label>
                                            <input class="form-control" type="text" value={this.state.city} onChange={this.handleCity} required/>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter state : </label>
                                            <input class="form-control" type="text" value={this.state.astate} onChange={this.handleAstate} required/>
                                        </div>
                                            <input class="btn btn-primary" type="submit" value="Search" />
                                        </form><br/><br/><br/>
                                    </div>);
                    case 2:   return(<div>
                                        <form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.getDoctorsBySpec}>
                                        <div class="form-group">
                                            <label>Enter Speciality : </label>
                                            <input class="form-control" type="text" value={this.state.speciality} onChange={this.handleSpeciality} required/>
                                        </div>
                                            <input class="btn btn-primary" type="submit" value="Search" />
                                        </form><br/><br/><br/>
                                        
                                    </div>);
                                    }
                })()}
                
                {
                    showDoctors ? <TableContainer component={Paper}>
                                    <Table  aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Name</TableCell>
                                                <TableCell>Experience</TableCell>
                                                <TableCell>Speciality</TableCell>
                                                <TableCell>Get Availability</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {doctors.length ? this.displayDoctors(doctors) : <TableRow><TableCell align="right">No doctors available</TableCell></TableRow>}
                                        </TableBody>
                                    </Table>
                                </TableContainer> : null
                }
                <br/><br/><br/>
                {
                    showDoctorAvailability ? <TableContainer component={Paper}>
                    <Table  aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Date</TableCell>
                                <TableCell>Time</TableCell>
                                <TableCell>Location</TableCell>
                                <TableCell>Book Appointment</TableCell>
                                <TableCell>Share Profile</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>{doctorAvailability.length ? this.displayDoctorAvailability(doctorAvailability) : <TableRow><TableCell align="right">Sorry this doctor has no availabilities</TableCell></TableRow>}</TableBody>
                    </Table>
                </TableContainer> : null
                }
                <br/><br/><br/>
                {
                    checkAppointment ? <div><b>Appointment is booked</b></div> : null
                }
                <br/><br/><br/>
            </div>
        )
    }
}

export default SearchDoctor;
