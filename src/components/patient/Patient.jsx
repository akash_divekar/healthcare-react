import React from 'react';
import './Patient.css';
import { Button } from 'antd';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
    } from "react-router-dom";
import UserProfile from '../../UserProfile';
import SearchDoctor from './SearchDoctor';
import DisplayAppointments from './DisplayAppointments';
import ViewProfile from './ViewProfile';
import Seminar from './Seminar';
import Axios from 'axios';
import AllApi from '../../api/AllApi';

class Patient extends React.Component {

    state = {
        patientName: null
    }

    componentDidMount = () => {
        this.setState({patientName:UserProfile.getName()});
    }

    logout = async() => {
        UserProfile.setName(null);
        UserProfile.setId(null);
        UserProfile.setRole(null);
        const request = AllApi.logout();
        const response = await Axios.post(request.url);
        console.log(response.data);
        this.props.history.push('/');
    }

    render() {

        const { patientName } = this.state;
        return(
            <div className="patient-wrapper">
                <Router>
                <div style={{float:'left', fontSize:'30px'}}><b>Patient</b></div>
                <div style={{float:'right', fontSize:'15px', marginRight:'20px'}}><b>Name : {patientName}</b></div><br/>
                <div className="patient-button-wrapper">
                    <Link to="/displayProfiles"><Button className="patient-button">Profile</Button></Link>
                    <Link to='/displayAppointments'><Button className="patient-button">My Appointments</Button></Link>
                    <Link to='/searchDoctor'><Button className="patient-button">Book Appointments</Button></Link>
                    <Link to='/bookSessions'><Button className="patient-button" style={{marginBottom:'2%'}}>Book Sessions</Button></Link>
                    <Button className="patient-button" onClick={this.logout}>Logout</Button>
                    <Switch>
                        <Route exact path="/searchDoctor" component={SearchDoctor} />
                        <Route exact path="/displayAppointments" component={DisplayAppointments} />
                        <Route exact path="/displayProfiles" component={ViewProfile} />
                        <Route exact path="/bookSessions" component={Seminar} />
                    </Switch>
                </div>
                </Router>
            </div>
        )
    }
}

export default Patient;