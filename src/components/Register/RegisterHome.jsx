import React from 'react';
import axios from 'axios';
import { Button } from 'antd';

import RegisterforPatient from './RegisterforPatient';
import RegisterforDoctor from './RegisterforDoctor';
import RegisterforHospitaladmin from './RegisterforHospitaladmin';

import { Link, Switch, Route, BrowserRouter as Router } from "react-router-dom";

class RegisterHome extends React.Component{

back = () =>{
	this.props.history.push("/");
}

render(){
	return(
	<Router>
	<div className="RegisterWrapper">
	<Button className="register-button" onClick={this.back}>Back</Button>
	<div className="RegisterHome">
	<Link to="/registerforpatient"><Button className="register-button">Patient</Button></Link>
	<Link to="/registerfordoctor"><Button className="register-button">Doctor</Button></Link>
	<Link to="/registerforhospitaladmin"><Button className="register-button">Hospital admin</Button></Link>
	<Switch>
		<Route exact path="/registerforpatient" component={RegisterforPatient} />
        <Route exact path="/registerfordoctor" component={RegisterforDoctor} />
        <Route exact path="/registerforhospitaladmin" component={RegisterforHospitaladmin} />
	</Switch>
	</div>
	</div>
	</Router>
	)
}
}
export default RegisterHome;