import { Button } from 'antd';
import React from 'react';
import AddDoctorToHospital from './AddDoctorToHospital';
import './HospitalAdmin.css';
import RemoveDoctorFromHospital from './RemoveDoctorFromHospital';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
    } from "react-router-dom";
import AddSession from './AddSession';
import UserProfile from '../../UserProfile';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import HospitalReview from './HospitalReview';

class HospitalAdmin extends React.Component {

    state =  {
        hospitalAdminName : null
    }

    componentDidMount =() => {
        this.setState({hospitalAdminName:UserProfile.getName()});
    }

    logout = async() => {
        UserProfile.setName(null);
        UserProfile.setId(null);
        UserProfile.setRole(null);
        const request = AllApi.logout();
        const response = await Axios.post(request.url);
        console.log(response.data);
        this.props.history.push('/');
    }

    render() {

        const { hospitalAdminName } = this.state;
        return (
            <div className="hospitalAdmin-wrapper">
                <Router>
                <div style={{float:'left', fontSize:'30px'}}><b>Hospital Admin</b></div>
                <div style={{float:'right', fontSize:'15px', marginRight:'20px'}}><b>Name : {hospitalAdminName}</b></div><br/>
                <div className="hospitalAdmin-button-wrapper">
                <Link to='/addDoctorToHospital'><Button className="hospitalAdmin-button">Add Doctor</Button></Link>
                <Link to='/removeDoctorFromHospital'><Button className="hospitalAdmin-button">Remove Doctor</Button></Link>
                <Link to='/addSession'><Button className="hospitalAdmin-button">Add Session</Button></Link>
                <Link to='/viewHospitalReview'><Button className="hospitalAdmin-button">View Reviews</Button></Link>
                <Button className="patient-button" onClick={this.logout}>Logout</Button>
                </div>
                <Switch>
                    <Route exact path="/addDoctorToHospital" component={AddDoctorToHospital} />
                    <Route exact path="/removeDoctorFromHospital" component={RemoveDoctorFromHospital} /> 
                    <Route exact path="/addSession" component={AddSession} /> 
                    <Route exact path="/viewHospitalReview" component={HospitalReview} />
                </Switch>
                </Router>
            </div>
        )
    }

}

export default HospitalAdmin;