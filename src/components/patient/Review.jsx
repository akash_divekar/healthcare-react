import React from 'react';
import './ViewProfile.css';
import {Button} from 'antd';
import Axios from 'axios'
import UserProfile from '../../UserProfile';
import AllApi from '../../api/AllApi';

class Review extends React.Component {

    state = {
        hospitalId:null,
        doctorId:null,
        rating: null,
        reviewText:'',
        resDoctor:null,
        resHospital:null,
        checkReview : false,
    }

    setHospitalId = () => {
        this.setState({hospitalId:this.props.hospitalId});
        this.setState({doctorId:null});
        this.setState({checkReview:false});
        this.setState({resDoctor:null});
        this.setState({resHospital:null});
    }

    setDoctorId = () => {
        this.setState({doctorId:this.props.doctorId});
        this.setState({hospitalId:null});
        this.setState({checkReview:false});
        this.setState({resDoctor:null});
        this.setState({resHospital:null});
    }

    handleRating = (e) => {
        this.setState({rating:e.target.value});
    }

    handleReviewText = (e) => {
        this.setState({reviewText:e.target.value});
    }

    addReview = async(e) => {
        this.setState({checkReview:true});
        e.preventDefault();
        console.log("rating",this.state.rating);
        console.log("review",this.state.reviewText);
        if(this.state.hospitalId) {
            const request = AllApi.reviewHospital();
            const response = await Axios.post(request.url,
            {"apid":this.props.appointmentId,"rating":this.state.rating,"review":this.state.reviewText},
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,
            'id':UserProfile.getId()}});
            this.setState({resHospital:response.data});
        }
        else {
            const request = AllApi.reviewDoctor();
            const response = await Axios.post(request.url,
            {"apid":this.props.appointmentId,"rating":this.state.rating,"review":this.state.reviewText},
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,
            'id':UserProfile.getId()}});
            this.setState({resDoctor:response.data});
        }
        this.setState({rating:''});
        this.setState({reviewText:''});
    }

    render() {

        const { doctorId,hospitalId,rating,reviewText,resDoctor,resHospital,checkReview } = this.state;

        return (
            <div>
                <h1>Review</h1>
                <Button style={{margin:'1%'}} onClick={() => this.setHospitalId()}>For Hospital</Button>
                <Button style={{margin:'1%'}} onClick={() => this.setDoctorId()}>For Doctor</Button><br/><br/>
                {
                    (doctorId || hospitalId) ? 
                    <div>
                        <form style={{"width":"50%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.addReview}>
                            <div class="form-group">
                                <label>Rating(Enter any number between 1 & 5)</label>
                                <input type="number" class="form-control" value={rating} onChange={this.handleRating} placeholder="Rating" min="1" max="5" required/>
                            </div>
                            <div class="form-group">
                                <label>Review</label>
                                <textarea class="form-control" value={reviewText} onChange={this.handleReviewText} placeholder="Type your review" required></textarea>
                            </div>
                            <input type="submit" class="btn btn-primary"></input>
                        </form>
                        <br/>
                    </div>
                    : 
                    <div></div>
                }
                <br/>
                {
                    checkReview ? 
                    <div>{(resDoctor || resHospital) ? <div><b>Review added successfully</b></div> : <div>You have already given the review for this appointment</div>}</div> 
                    : 
                    <div></div>
                }
                <br/><br/>
            </div>
        )
    }
}

export default Review;