import { Button } from 'antd';
import React from 'react';
import './SystemAdmin.css';
import Urls from '../../Urls/Urls';
import axios from 'axios';
import UserProfile from '../../UserProfile';

import { Link, Switch, Route, BrowserRouter as Router } from "react-router-dom";

import AddHospital from './AddHospital';
import RemoveHospital from './RemoveHospital';
import AddHospitalAdmin from './AddHospitalAdmin';
import RemoveHospitalAdmin from './RemoveHospitalAdmin';

class SystemAdmin extends React.Component {

    logout = async() =>{
        const req = Urls.logoutprofile();
		const res =  await axios.post(req.url);
        this.props.history.push("/");
    }

    render() {
 
        return (
        <Router>
            <div className="systemAdmin-wrapper">
                <div className="systemAdmin-heading">System Admin</div>
                <div style={{textAlign:'right', marginRight:'2%'}} >
                Name : {UserProfile.getName()}
                </div>
                <Link to="/addhospital"><Button className="systemAdmin-button" >Add Hospital</Button></Link>
                <Link to="/removehospital"><Button className="systemAdmin-button" >Remove Hospital</Button></Link>
                <Link to="/addhospitaladmin"><Button className="systemAdmin-button" >Add Hospital Admin</Button></Link>
                <Link to="/removehospitaladmin"><Button className="systemAdmin-button" >Remove Hospital Admin</Button></Link>
                <Button className="systemAdmin-button" onClick={this.logout}>Logout</Button>
                <Switch>
                    <Route exact path="/addhospital" component={AddHospital} />
                    <Route exact path="/removehospital" component={RemoveHospital} />
                    <Route exact path="/addhospitaladmin" component={AddHospitalAdmin} />
                    <Route exact path="/removehospitaladmin" component={RemoveHospitalAdmin} />
                </Switch>
            </div>

        </Router>
        )
    }

}

export default SystemAdmin;