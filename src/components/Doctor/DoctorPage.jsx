import { Button } from 'antd';
import React from 'react';
import './DoctorPage.css';
import Appointments from './Appointments';
import Availability from './Availability';
import Review from './Review';
import { Link } from "react-router-dom";
import axios from 'axios';
import Urls from '../../Urls/Urls';

import UserProfile from '../../UserProfile';

class DoctorPage extends React.Component {

    state =  {
        doctorName: null,
        show : false,
        ashow : false,
        review : false,
    }
    showappointments = () =>{
        this.setState({show:true,ashow:false,review:false});
    }

    showavailability = () =>{
        this.setState({ashow:true,show:false,review:false})
    }

    showreview = () =>{
        this.setState({review:true,show:false,ashow:false})
    }

    logout = async() =>{
        const req = Urls.logoutprofile();
		const res =  await axios.post(req.url);
        this.props.history.push("/");
    }

    render() {

        const { doctorName, show } = this.state;
 
        return (
            <div className="doctor-wrapper">
                <div className="doctor-heading">Doctor</div>
                <div style={{textAlign:'right', marginRight:'2%'}} >
                
                <b>Name : {UserProfile.getName()}</b>
                </div>
                <Button style={{margin:'1% 0% 2% 2%'}} onClick={this.showappointments}>Appointments</Button>
                <Button style={{margin:'1% 0% 2% 2%'}} onClick={this.showavailability}>Availability</Button>
                <Button style={{margin:'1% 0% 2% 2%'}} onClick={this.showreview}>Reviews</Button>
                <Link to="/"><Button style={{margin:'1% 0% 2% 2%'}} onClick={this.logout}>Logout</Button></Link>
                {this.state.show?<Appointments />:<div></div>}
                {this.state.ashow?<Availability />:<div></div>}
                {this.state.review?<Review />:<div></div>}
            </div>
        )
    }
}

export default DoctorPage;