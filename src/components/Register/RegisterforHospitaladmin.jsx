import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import './Styles.css';

import Urls from '../../Urls/Urls';

class RegisterforHospitaladmin extends React.Component{

state={values:null,username:'',name:'',password:'',checkregister:false}

changeofusername = event =>{
	this.setState({username:event.target.value});
}

changeofname = event =>{
	this.setState({name:event.target.value});
}

changeofpassword = event =>{
	this.setState({password:event.target.value});
}

submittingthedetailes = async(event) =>{
	event.preventDefault();
		const req = Urls.getHospitaladmins();
		const res =  await axios.post(req.url,{
		userName:this.state.username,password:this.state.password,roles:"ROLE_HOSADMIN",name:this.state.name
		});
		console.log(res.data);
		this.setState({values:res.data,checkregister:true});
}

render(){
	return(
	<div className="RegisterforHospitaladmin">
	<form style={{"width":"50%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}}>
	<h1>Register Hospital Admin</h1>
	<label style={{color:''}}>Username : </label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofusername}/><br /><br />
	<label style={{color:''}}>Name : </label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofname}/><br /><br />
	<label style={{color:''}}>Password : </label>
	<input type="password" class="form-control" placeholder="Password..." onChange={this.changeofpassword} /><br /><br />
	<button type="submit" class="btn btn-primary" style={{textAlign:'center'}} onClick={this.submittingthedetailes}>Register</button>
	</form>
	{this.state.checkregister ? <div>{this.state.values ? <div>Hospital Admin registered successfully</div>: <div style={{color:'red'}}>UserName already exists</div>}</div> : <div></div>}
	</div>
	)
	}
	}

export default RegisterforHospitaladmin;