import React from 'react';
import axios from 'axios';
import Urls from '../../Urls/Urls';
import './SystemAdmin.css';

import UserProfile from '../../UserProfile';

class AddHospital extends React.Component{

state={values:null,name:'',area:'',city:'',state:''}

changeofname = event =>{
	this.setState({name:event.target.value});
}

changeofarea = event =>{
	this.setState({area:event.target.value});
}

changeofcity = event =>{
	this.setState({city:event.target.value});
}
	
changeofstate = event =>{
	this.setState({state:event.target.value});
}

submittingthedetailes = async(event) =>{
	event.preventDefault();
		const req = Urls.addnewhospital();
		const res =  await axios.post(req.url,{name:this.state.name,area:this.state.area,city:this.state.city,state:this.state.state},
		{headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}}
		);
		console.log(res.data);
		this.setState({values:res.data});
}

render(){
	return(
	<div className="AddHospital">
	<h1>Add Hospital</h1>
	<form style={{"width":"40%","margin":"10px","border":"1px solid grey","padding":"10px","border-radius":"5px"}} onSubmit={this.submittingthedetailes}>
	<label style={{color:''}}>Name :</label>
	<input type="text" class="form-control" placeholder="Name..." onChange={this.changeofname} /><br /><br />
	<label style={{color:''}}>Area :</label>
	<input type="text" class="form-control" placeholder="Area..." onChange={this.changeofarea} /><br /><br />
	<label style={{color:''}}>City : </label>
	<input type="text" class="form-control" placeholder="City..." onChange={this.changeofcity} /><br /><br />
	<label style={{color:''}}>State : </label>
	<input type="text" class="form-control" placeholder="State..." onChange={this.changeofstate} /><br /><br />
	<button type="submit" class="btn btn-primary">Add</button>
	</form>
	{this.state.values == null ? <div></div> : <div>Hospital Added Successfully</div>}
	</div>
	)
	}
	}

export default AddHospital;