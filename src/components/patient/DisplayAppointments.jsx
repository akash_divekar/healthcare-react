import React from 'react';
import './DisplayAppointments.css';
import Axios from 'axios';
import AllApi from '../../api/AllApi';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import UserProfile from '../../UserProfile';
import { Button } from 'antd';
import Review from './Review';

class DisplayAppointments extends React.Component {

    state = {
        appointments : [],
        apIdForReview : null,
        dIdForReview: null,
        hIdForReview: null,
        pIdForReview: null,
    }

    componentDidMount = () => {
        this.fetchAppointments();
    }

    fetchAppointments = async() => {
        const request = AllApi.fetchPatientAppointments();
        const response = await Axios.get(request.url,
            {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`,'id':UserProfile.getId()}});
        this.setState({appointments:response.data});
    }

    cancelAppointment = async(appointmentId) => {
        const request = AllApi.patientCancelsAppointment();
        await Axios.delete(`${request.url}/${appointmentId}`, {headers:{'Authorization':`Basic ${UserProfile.getEncodedValue()}`}});
        this.fetchAppointments();
    }

    addReview = (appointmentId,doctorId,hospitalId,patientId) => {
        console.log("entry Id",appointmentId);
        console.log(this.state.apIdForReview);
        this.setState({apIdForReview:appointmentId});
        this.setState({dIdForReview:doctorId});
        this.setState({hIdForReview:hospitalId});
        this.setState({pIdForReview:patientId});
        console.log(this.state.apIdForReview);
    }

    displayAppointments = (appointments) => (
        appointments.map(appointment=>{
        return(
            <TableRow key={appointment.id}>
                <TableCell>{appointment.date}</TableCell>
                <TableCell>{appointment.time}</TableCell>
                <TableCell>{appointment.doctor.name}</TableCell>
                <TableCell>{appointment.hospital.name}</TableCell>
                <TableCell>{appointment.hospital.area},{appointment.hospital.city},{appointment.hospital.state}</TableCell>
                <TableCell>{appointment.done ? <div>done</div> : <div>pending</div>}</TableCell>
                <TableCell>{appointment.done ? <Button type="primary" onClick={() => this.addReview(appointment.id,appointment.doctor.id,appointment.hospital.id,appointment.patient.id)}>Review</Button> : <Button type="primary" onClick={() => this.cancelAppointment(appointment.id)}>Cancel</Button>}</TableCell>
            </TableRow>
        ) 
        })
    )

    render() {

        const { appointments,apIdForReview,dIdForReview,hIdForReview,pIdForReview } = this.state; 

        return(
            <div className="displayAppointments-wrapper">
                <h1>Appointments</h1>
                <TableContainer component={Paper}>
                <Table  aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell>Time</TableCell>
                        <TableCell>Doctor</TableCell>
                        <TableCell>Hospital</TableCell>
                        <TableCell>Location</TableCell>
                        <TableCell>Status</TableCell>
                        <TableCell>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {appointments.length ? this.displayAppointments(appointments) : <TableRow><TableCell align="right">No Appointments</TableCell></TableRow>}
                </TableBody>
                </Table>
            </TableContainer><br/><br/>
            {
                apIdForReview ? <Review appointmentId = {apIdForReview} doctorId = {dIdForReview} hospitalId = {hIdForReview} patientId = {pIdForReview}></Review> : <div></div>
            }
            </div>
        )
    }
}

export default DisplayAppointments;